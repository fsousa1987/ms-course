package com.francisco.hrpayroll.resources;

import com.francisco.hrpayroll.entities.Payment;
import com.francisco.hrpayroll.services.PaymentService;
import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import io.github.resilience4j.timelimiter.annotation.TimeLimiter;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.CompletableFuture;

@RestController
@RequestMapping(value = "/payments")
public class PaymentResource {

    private static final String PAYMENT_SERVICE = "paymentService";
    private static final String FALLBACK_METHOD = "paymentFallback";

    private final PaymentService service;

    public PaymentResource(PaymentService service) {
        this.service = service;
    }

    @GetMapping(value = "/{workerId}/days/{days}")
    @CircuitBreaker(name = PAYMENT_SERVICE, fallbackMethod = FALLBACK_METHOD)
    @TimeLimiter(name = PAYMENT_SERVICE, fallbackMethod = FALLBACK_METHOD)
    public CompletableFuture<Payment> getPayment(@PathVariable Long workerId, @PathVariable Integer days) {
        return CompletableFuture.supplyAsync(() -> service.getPayment(workerId, days));
    }

    public CompletableFuture<Payment> paymentFallback(@PathVariable Long workerId, @PathVariable Integer days, Exception e) {
        Payment payment = new Payment("Brann", 400.0, days);
        return CompletableFuture.supplyAsync(() -> payment);
    }
}
