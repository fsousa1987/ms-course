package com.francisco.hrpayroll.services;

import com.francisco.hrpayroll.entities.Payment;
import com.francisco.hrpayroll.entities.Worker;
import com.francisco.hrpayroll.feignclients.WorkerFeignClient;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service
public record PaymentService(WorkerFeignClient workerFeignClient) {

    public Payment getPayment(long workerId, int days) {
        Worker worker = workerFeignClient.findById(workerId).getBody();
        return new Payment(Objects.requireNonNull(worker).getName(), worker.getDailyIncome(), days);
    }
}
